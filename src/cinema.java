/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author sebas
 */
public class cinema {
    private int boleto;
    private int combo;
    private String asientos;
    private String pelicula;
    
    public cinema(){
        this.boleto=0;
        this.combo=0;
        this.asientos="";
        this.pelicula="";
    }

    public cinema(int boleto, int combo, String asientos, String pelicula) {
        this.boleto = boleto;
        this.combo = combo;
        this.asientos = asientos;
        this.pelicula = pelicula;
    }
    
    public cinema(cinema otro){
        this.boleto = otro.boleto;
        this.combo = otro.combo;
        this.asientos = otro.asientos;
        this.pelicula = otro.pelicula;
    }

    public int getBoleto() {
        return boleto;
    }

    public void setBoleto(int boleto) {
        this.boleto = boleto;
    }

    public int getCombo() {
        return combo;
    }

    public void setCombo(int combo) {
        this.combo = combo;
    }

    public String getAsientos() {
        return asientos;
    }

    public void setAsientos(String asientos) {
        this.asientos = asientos;
    }

    public String getPelicula() {
        return pelicula;
    }

    public void setPelicula(String pelicula) {
        this.pelicula = pelicula;
    }
    
    public float calcularPagoBoleto(){
        int bol = this.boleto;
        float totalBol=0.0f;
        float pago = 0.0f;
        float debul = 0.0f;
        totalBol= bol * 50; 
        debul = pago - totalBol;
        return totalBol;
        
    }
    
    public float calcularPagoBoleto(int numBoletos) {
    return numBoletos * 50.0f; // el precio del boleto es de 50
}
      
    public float calcularPagoCombo(){
    float pago = 0.0f;
    float combo1 = 250;
    float combo2 = 320;
    float combo3 = 440;
    
    if (this.combo == 1) {
        pago = combo1;
    } else if (this.combo == 2) {
        pago = combo2;
    } else if (this.combo == 3) {
        pago = combo3;
    } else {
        System.out.println("Combo inexistente");
        return 0.0f; 
    }
    
    return pago;
    }
    
    
    
}
