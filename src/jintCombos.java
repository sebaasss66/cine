
import javax.swing.JOptionPane;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author sebas
 */
public class jintCombos extends javax.swing.JFrame {

    /**
     * Creates new form jintCombos
     */
    public jintCombos() {
        initComponents();
        setSize(850,600);
        
        this.deshabilitar();
    }

    public void deshabilitar(){
        this.cmbCombo.setEnabled(false);
        this.txtTotal.setEnabled(false);
        this.txtPagar.setEnabled(false);
        this.txtCambio.setEnabled(false);
        this.btmGuardar.setEnabled(false);
        this.btmMostrar.setEnabled(false);
    }
    
    public void habilitar(){
        this.cmbCombo.setEnabled(!false);
        this.txtTotal.setEnabled(false);
        this.txtPagar.setEnabled(!false);
        this.txtCambio.setEnabled(false);
        this.btmGuardar.setEnabled(true);
        this.btmNuevo.setEnabled(true);
        this.btmLimpiar.setEnabled(true);
        this.btmMostrar.setEnabled(true);
    }
    
    public void limpiar(){
        this.cmbCombo.setSelectedIndex(0);
        this.txtTotal.setText("");
        this.txtPagar.setText("");
        this.txtCambio.setText("");
    }
    
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        cmbCombo = new javax.swing.JComboBox<>();
        jLabel3 = new javax.swing.JLabel();
        txtTotal = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        txtPagar = new javax.swing.JTextField();
        txtCambio = new javax.swing.JTextField();
        jPanel2 = new javax.swing.JPanel();
        btmRegresar = new javax.swing.JButton();
        btmNuevo = new javax.swing.JButton();
        btmMostrar = new javax.swing.JButton();
        btmGuardar = new javax.swing.JButton();
        btmLimpiar = new javax.swing.JButton();
        btmCancelar = new javax.swing.JButton();
        btmCerrar = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        getContentPane().setLayout(null);

        jLabel1.setFont(new java.awt.Font("Verdana", 2, 24)); // NOI18N
        jLabel1.setText("Combos ");
        getContentPane().add(jLabel1);
        jLabel1.setBounds(350, 10, 110, 20);

        jPanel1.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel1.setLayout(null);

        jLabel2.setFont(new java.awt.Font("Verdana", 2, 13)); // NOI18N
        jLabel2.setText("Seleccione su combo");
        jPanel1.add(jLabel2);
        jLabel2.setBounds(10, 20, 140, 17);

        cmbCombo.setFont(new java.awt.Font("Verdana", 2, 13)); // NOI18N
        cmbCombo.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "combo 1 ", "combo 2", "combo 3" }));
        jPanel1.add(cmbCombo);
        cmbCombo.setBounds(160, 20, 91, 23);

        jLabel3.setFont(new java.awt.Font("Verdana", 2, 13)); // NOI18N
        jLabel3.setText("Total a pagar:");
        jPanel1.add(jLabel3);
        jLabel3.setBounds(10, 60, 96, 17);
        jPanel1.add(txtTotal);
        txtTotal.setBounds(110, 60, 90, 22);

        jLabel4.setFont(new java.awt.Font("Verdana", 2, 13)); // NOI18N
        jLabel4.setText("Ingrese con cuanto va a pagar:");
        jPanel1.add(jLabel4);
        jLabel4.setBounds(10, 120, 213, 17);

        jLabel5.setFont(new java.awt.Font("Verdana", 2, 13)); // NOI18N
        jLabel5.setText("Su cambio:");
        jPanel1.add(jLabel5);
        jLabel5.setBounds(10, 160, 75, 20);
        jPanel1.add(txtPagar);
        txtPagar.setBounds(230, 120, 90, 22);
        jPanel1.add(txtCambio);
        txtCambio.setBounds(90, 160, 90, 22);

        getContentPane().add(jPanel1);
        jPanel1.setBounds(370, 130, 430, 210);

        jPanel2.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel2.setLayout(null);
        getContentPane().add(jPanel2);
        jPanel2.setBounds(20, 60, 310, 350);

        btmRegresar.setFont(new java.awt.Font("Verdana", 2, 13)); // NOI18N
        btmRegresar.setText("Regresar");
        btmRegresar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btmRegresarActionPerformed(evt);
            }
        });
        getContentPane().add(btmRegresar);
        btmRegresar.setBounds(710, 470, 100, 25);

        btmNuevo.setFont(new java.awt.Font("Verdana", 2, 13)); // NOI18N
        btmNuevo.setText("Nuevo");
        btmNuevo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btmNuevoActionPerformed(evt);
            }
        });
        getContentPane().add(btmNuevo);
        btmNuevo.setBounds(500, 70, 80, 25);

        btmMostrar.setFont(new java.awt.Font("Verdana", 2, 13)); // NOI18N
        btmMostrar.setText("Mostrar");
        btmMostrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btmMostrarActionPerformed(evt);
            }
        });
        getContentPane().add(btmMostrar);
        btmMostrar.setBounds(710, 70, 90, 25);

        btmGuardar.setFont(new java.awt.Font("Verdana", 2, 13)); // NOI18N
        btmGuardar.setText("Guardar");
        btmGuardar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btmGuardarActionPerformed(evt);
            }
        });
        getContentPane().add(btmGuardar);
        btmGuardar.setBounds(600, 70, 90, 25);

        btmLimpiar.setFont(new java.awt.Font("Verdana", 2, 13)); // NOI18N
        btmLimpiar.setText("Limpiar");
        btmLimpiar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btmLimpiarActionPerformed(evt);
            }
        });
        getContentPane().add(btmLimpiar);
        btmLimpiar.setBounds(380, 470, 90, 25);

        btmCancelar.setFont(new java.awt.Font("Verdana", 2, 13)); // NOI18N
        btmCancelar.setText("Cancelar");
        btmCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btmCancelarActionPerformed(evt);
            }
        });
        getContentPane().add(btmCancelar);
        btmCancelar.setBounds(500, 470, 91, 25);

        btmCerrar.setFont(new java.awt.Font("Verdana", 2, 13)); // NOI18N
        btmCerrar.setText("Cerrar");
        btmCerrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btmCerrarActionPerformed(evt);
            }
        });
        getContentPane().add(btmCerrar);
        btmCerrar.setBounds(610, 470, 75, 25);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btmRegresarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btmRegresarActionPerformed
        // TODO add your handling code here:
        menuCine ventana = new menuCine();
        ventana.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_btmRegresarActionPerformed

    private void btmGuardarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btmGuardarActionPerformed
        // TODO add your handling code here:
       boolean exito = false;
    if (cmbCombo.getSelectedIndex() == -1) exito = true;

    if (exito == true) {
        JOptionPane.showMessageDialog(this, "Faltó capturar información", "Advertencia", JOptionPane.WARNING_MESSAGE);
    } else {
        // Crear una instancia de la clase cinema
        cinema combo = new cinema();

        // Establecer el combo seleccionado en la instancia de cinema
        combo.setCombo(cmbCombo.getSelectedIndex() + 1);

        // Calcular el pago del combo seleccionado
        float pagoCombo = combo.calcularPagoCombo();
        
       int numBoletos = cmbCombo.getSelectedIndex() + 1;

    // Calcular el precio total por los boletos
    float totalPagar = combo.calcularPagoBoleto(numBoletos);

    // mostrar el total a pagar
    txtTotal.setText(String.format("%.2f", totalPagar));
    }
        
    }//GEN-LAST:event_btmGuardarActionPerformed

    private void btmNuevoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btmNuevoActionPerformed
        // TODO add your handling code here:
        combo = new cinema();
        this.habilitar();
    }//GEN-LAST:event_btmNuevoActionPerformed

    private void btmMostrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btmMostrarActionPerformed
        // TODO add your handling code here:
        if(!txtTotal.getText().isEmpty() && !txtPagar.getText().isEmpty() ){
            float pagar = Float.parseFloat(txtPagar.getText());
            float total = Float.parseFloat(txtTotal.getText());
            
            float cambio = pagar - total;
            txtCambio.setText(cambio >= 0 ? String.format("%.2f",cambio) : "no hay suficiente pago");
        }
        
    }//GEN-LAST:event_btmMostrarActionPerformed

    private void btmLimpiarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btmLimpiarActionPerformed
        // TODO add your handling code here:
        this.cmbCombo.setSelectedIndex(0);
        this.txtTotal.setText("");
        this.txtPagar.setText("");
        this.txtCambio.setText("");
    }//GEN-LAST:event_btmLimpiarActionPerformed

    private void btmCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btmCancelarActionPerformed
        // TODO add your handling code here:
        this.limpiar();
        this.deshabilitar();
    }//GEN-LAST:event_btmCancelarActionPerformed

    private void btmCerrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btmCerrarActionPerformed
        // TODO add your handling code here:
        int opcion = 0;
        opcion = JOptionPane.showConfirmDialog(this, "Deseas salir",
            "Pago Combos", JOptionPane.YES_NO_OPTION);
        if (opcion == JOptionPane.YES_OPTION){
            this.dispose();

        }
    }//GEN-LAST:event_btmCerrarActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(jintCombos.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(jintCombos.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(jintCombos.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(jintCombos.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new jintCombos().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btmCancelar;
    private javax.swing.JButton btmCerrar;
    private javax.swing.JButton btmGuardar;
    private javax.swing.JButton btmLimpiar;
    private javax.swing.JButton btmMostrar;
    private javax.swing.JButton btmNuevo;
    private javax.swing.JButton btmRegresar;
    private javax.swing.JComboBox<String> cmbCombo;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JTextField txtCambio;
    private javax.swing.JTextField txtPagar;
    private javax.swing.JTextField txtTotal;
    // End of variables declaration//GEN-END:variables
    private cinema combo = new cinema();
}
